---
title: "Managing TLS Certificates Using Ansible and Let’s Encrypt!:"
date: "2024-06-23"
description: "This tutorial will guide you through the basics of using Ansible Core to request a Let's Encrypt certificate, securely storing your Cloudflare API key and DNS Zone ID using Ansible Vault. This is the first part of a series on managing SSL certificate lifecycles using Ansible, progressing from Ansible Core to advanced automation with AWX, Ansible Automation Platform, and integration with GitOps and ServiceNow."
tags:
  - ansible
  - certificates
  - automation
  - letsencrypt
draft: false
---

# Managing TLS Certificates Using Ansible and Let’s Encrypt!

This tutorial series will guide you through the basics of using Ansible Core to request a Let's Encrypt certificate, securely storing your Cloudflare API key and DNS Zone ID using Ansible Vault. The series will cover managing SSL certificate lifecycles using Ansible, progressing from Ansible Core to advanced automation with AWX, Ansible Automation Platform, and integration with GitOps and ServiceNow.

## Prerequisites

- **Ansible Core** installed on your control node.
- **Ansible Vault** enabled.
- **Cloudflare account** with API token and DNS Zone ID for the zone your using to create the Certificate.
  - you can use this endpoint to retrieve your DNS Zone ID: https://api.cloudflare.com/client/v4/zones/
- Basic knowledge of **Ansible** and **YAML** syntax.

For this tutorial series I will be using **Ubuntu 22.04.4 LTS**. This can be installed on a standalone VM/server, run on windows via WSL or on a docker container.

## Incuded in this series

- **Part 1**: Basics of Using Ansible Core to Request a Let's Encrypt Certificate
- **Part 2**: Managing Certificates with AWX/Ansible Automation Platform with GitOps
- **Part 3**: Scheduling Auto-Renewal workflows and automating the deployment of certificates to Firewalls and F5's
- **Part 4**: ServiceNow Integration
- **BONUS** : Using Thousandeyes with Event Driven Ansible to renew Certs before they Expire.